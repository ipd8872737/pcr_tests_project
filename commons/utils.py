import io
import math
import string
import zipfile
from itertools import product

import pandas as pd


def zip_it(plot, excel):
    """
    A function that takes a png and an excel file.
    Then, return a buffer containing a zip file containing
    the plot and the excel files.
    """
    buffer = io.BytesIO()
    with zipfile.ZipFile(
            buffer, "a",zipfile.ZIP_DEFLATED, False
    ) as zip_file:
        for file_name, data in [('plot_result.png', plot),
                                ('excel_result.xlsx', excel)]:
            zip_file.writestr(file_name, data.getvalue())
    buffer.seek(0)
    return buffer


def get_plot(_df):
    buffer = io.BytesIO()
    # plot figure
    # TODO: improve figure settings
    # https://pandas.pydata.org/pandas-docs/version/0.18/generated/pandas.DataFrame.plot.html
    ax = _df.plot(
        kind="scatter", grid=True,
        x="Patients", y="Ratio", s="Absorbance", c='DarkBlue'
    )
    ax.figure.savefig(buffer)
    buffer.seek(0)
    return buffer


def get_plot_bis(_df):
    buffer = io.BytesIO()
    # TODO: improve
    ax = _df.plot(
        kind="scatter", grid=True,
        x="Patients", y="Test results", c='DarkBlue'
    )
    ax.figure.savefig(buffer)
    buffer.seek(0)
    return buffer


def convert_to_excel(_df):
    buffer = io.BytesIO()
    with pd.ExcelWriter(buffer) as writer:
        _df.to_excel(writer, sheet_name="Sheet1")
    # ???
    buffer.seek(0)
    return buffer


def categorize_ratio_eliza(ratio):
    """
    Function used to categozize the Ratio.
    This categorization technique is used for Elisa tests
    tests results.
    """
    if ratio < 30:
        return 0
    if 30 <= ratio <= 40:
        return 2
    else:
        return 1


def categorize_ratio_wantai(ratio):
    """
    Function used to categozize the Ratio.
    This categorization technique is used for Wantai tests
    tests results.
    """
    return 0 if ratio < 1 else 1


def categorize_inhibition(inhibition):
    """
    Function used to categozize the inihibition percentage.
    This categorization technique is used for Seroneutralization tests results.
    """
    return 1 if inhibition > 30 else 0



def color_positivity(val):
    """
    Function used to return the right color (for the Excel cell).
    This function is used for IDvet tests results.
    """
    if val == 0:
        color = "red"
    elif val == 1:
        color = "green"
    else:
        color = "yellow"
    return f"background-color: {color}"


def apply_formatting(col):
    """
    Function used to apply a specific color to a specific column.
    This function is used to color all the cells inside the Positivity_IDvet column.
    This function is used for IDvet tests results.
    """
    if col.name == "Positivity_IDVet":
        return [color_positivity(v) for v in col.values]


def extract_patients_and_control_ids(input_file):
    """
    Function that receive an Excel file as input.
    The input should have a sheet named "Plan de plaque".
    The input will be parsed to extract and return:
    - Patients anon names
    - Controls
    """
    try:
        dfi = pd.read_excel(
            pd.ExcelFile(input_file.file), sheet_name="Plan de plaque", dtype=str
        )
    except ValueError as err:
        if str(err) == "Worksheet named 'Plan de plaque' not found":
            raise ValueError(
                "Upload Excel file containing a worksheet named 'Plan de plaque'"
            )

    dfi.dropna(axis=0, how="all", inplace=True)
    # clean empty cols
    dfi.dropna(axis=1, how="all", inplace=True)
    # this is the actual dataframe, rows and cols with something in it
    dfi = dfi.iloc[1:, 1:]
    # clean NaN cols again
    dfi.dropna(axis=1, how="all", inplace=True)
    # retrieve patients
    patients = []
    for column in dfi.columns:
        # take element after element, column after column
        patients.extend(dfi[column].tolist())

    # patients are everything until I find BLANC or keep everything except the nan values
    # patients = [x for x in takewhile(lambda x: x!="BLANC", patients)]
    # patients = [item for item in patients if not(math.isnan(item)) == True]
    patients = [item for item in patients if isinstance(item, str) is True]

    # blanc, neg, pos are the last 3
    # TODO: only for the IDvet Plate plan
    # TODO: not true for all the other Plate plans
    control_identifiers = patients[-3:]
    # samples are everything except the last 3
    patients = patients[:-3]
    return patients, control_identifiers


def extract_absorbance_and_control_values(output_file):
    """
    Function that receive an Excel file as input.
    The file should contain a sheet named 'Résultats d'absorbance'.
    This sheet contains all the pcr test results we need.
    """
    try:
        df_absorbance = pd.read_excel(
            pd.ExcelFile(output_file.file), "Résultats d'absorbance"
        )
    except ValueError as err:
        if str(err) == "Worksheet named 'Résultats d'absorbance' not found":
            raise ValueError(
                "Upload an Excel file containing a worksheet named 'Résultats d'absorbance'"
            )

    # extract actual absorbance data
    absorbance_data = df_absorbance.iloc[5:, 1:]
    # clean NaN cols again
    absorbance_data.dropna(axis=1, how="all", inplace=True)

    # transform the df to a list
    absorbance = []
    for column in absorbance_data.columns:
        absorbance.extend(absorbance_data[column].tolist())

    # keep everything except the nan values
    absorbance = [item for item in absorbance if (math.isnan(item)) is not True]

    # extract controls, the last 3 of the column
    control_values = absorbance[-3:]
    # then remove them
    absorbances = absorbance[:-3]
    return absorbances, control_values


def process_results(input_file, output_file):
    """
    Function used to process 2 Excel files.
    - input_file contains the PCR Plate plan
    - output_file contains the PCR test results

    This function works only for the IDvet tests,
    where the controls are positionned at the last 3 wells.
    """
    patients, control_ids = extract_patients_and_control_ids(input_file)
    absorbances, control_values = extract_absorbance_and_control_values(output_file)

    # define which one is white ? which one is neg ? which one is pos ?
    controls_dict = dict(zip(control_ids, control_values))
    blanc = controls_dict.get("BLANC")
    neg = controls_dict.get("NEG")
    pos = controls_dict.get("POS")

    # length of absorbance should be equal to length of patients
    if len(absorbances) != len(patients):
        raise ValueError(
            f"N° of patients: ({len(patients)}) != N° of results: ({len(absorbances)})"
        )

    df_result = pd.DataFrame(
        {
            "Patients": patients,
            "Absorbance": absorbances,
            "Contrôle négatif": [neg for _ in patients],
            "Contrôle positif": [pos for _ in patients],
        }
    )

    df_result["Ratio"] = (
        (df_result["Absorbance"] - df_result["Contrôle négatif"])
        / (df_result["Contrôle positif"] - df_result["Contrôle négatif"])
        * 100
    )
    df_result["Positivity_IDVet"] = df_result["Ratio"].apply(categorize_ratio_eliza)
    df_result["Patients"] = df_result["Patients"].astype(str)
    df_styled = df_result.style.map(color_positivity, subset=["Positivity_IDVet"])
    excel = convert_to_excel(df_styled)
    plot = get_plot(df_result)
    return excel, plot


def get_location_names():
    """
    Function used to return all the valid well names
    inside a PCR Plate. The names are returned inside a list.
    The names are returned column by column, row by row.
    """
    loc_names = [
        row+str(col) for col, row in product(
            range(1, 13), string.ascii_uppercase[:8]
        )]
    return loc_names


def divide_list_into_chunks(lst, chunk_size=8):
    lst2 = [lst[i:i+chunk_size] for i in range(0, len(lst), chunk_size)]
    return lst2

def skip_plate_values():
    """
    When parsing an Excel file containing a PCR Plate plan,
    We want to be able to skip letters used to name the rows.
    We want to be able to skip numbers used to name the cols.
    We want to be able to skip None values.
    """
    strings = list(string.ascii_uppercase[:8])
    numbers = list(range(1,13))
    numbers_str = list(map(lambda x: str(x), range(1,13)))
    null = [None]
    return strings + numbers + numbers_str + null


def read_result_file_into_list(excel_file):
    """
    Function used to read an Excel file containing
    spectrophotometer results, column by column.
    Then, return those results as a Python list of couples.
    Each couple is [location, absorbance value]
    """
    try:
        df = pd.read_excel(
            pd.ExcelFile(excel_file),
            "Résultats d'absorbance",
            nrows=9, # the Plate has 9 rows
            skiprows=[0,1,2,3,4], # the Excel file has some rows in the start that we don't need
        )
    except ValueError as err:
        if str(err) == "Worksheet named 'Résultats d'absorbancee' not found":
            raise ValueError(
                "Upload an Excel file containing a worksheet named 'Résultats d'absorbance'"
            )

    # drop the first column, the one containing the row names
    df.drop(df.columns[0], axis=1, inplace=True)
    results = []
    for col in df.columns:
        results.extend(df[col].tolist())
    return results
