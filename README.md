# Introduction

## 1.1 Purpose

The purpose of this document is to build an online system to automate the mapping of each result returned by a spectrophotometer to the correct patient, build a recap, draw some charts.

## 1.2 Intended Audience:

The intended and primary audience of this document are all the people at IPD labs dealing with PCR plate plans, all the people at the DDSI. This document will be written with no technical terms (_hopefully_) for the benefit of everyone on the team. It will define the business rules of this application.

## 1.3 Scope:

One of the chore at the IPD immunology lab is (_among other things_) to get the results returned by their [spectrophotometer](https://www.thermofisher.com/sn/en/home/life-science/lab-equipment/microplate-instruments/plate-readers/models/multiskan-skyhigh.html) and for each result, retrieve what is the associated patient. The patients samples are retrieved from a [96 wells PCR plate](https://www.azenta.com/products/96-well-skirted-pcr-plate). This task is actually done manually, using Excel files. the current project will try to ease this process.

# Overall Description of the product:

- Upload an Excel file containing a description of the PCR plate plan
- Upload an Excel file containing the results returned by the spectrophotometer
- Retrieve the resulting Excel file
- See a scatter plot of the results

## Installation and Usage:

To run the project locally, one may want to :

- git clone the repository: `git clone ...`
- cd into the repository `cd ...`
- create a Python virtualenv `python -m venv .env`
- activate the Python virtualenv `source .env/bin/activate`
- install Dependencies: `pip install -r requirements.txt`
- make sure to have PostgreSQL installed
- make sure to export the following environment variables to the correct values on your system:
  - "PGDATABASE"
  - "PGUSER"
  - "PGPWD"
  - "PGHOST"
  - "PGPORT"

- go inside the root directory then: 
  - `python manage.py createsuperuser`
  - `python manage.py migrate`
  - `python manage.py runserver`
- open a web browser and visit the following url: `http://localhost:8000/api/v1/schema/swagger-ui/`

