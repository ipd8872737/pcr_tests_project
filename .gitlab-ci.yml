# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

stages:
- test
- quality
- prod

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml


unittest:
  image: python:3.11.4-slim
  stage: test
  before_script:
    - pip install --no-cache-dir -r requirements/dev.txt
    - apt-get update -qq && apt-get install -y sqlite3
  script:
    - DJANGO_SETTINGS_MODULE=config.test_settings pytest
  when: manual


linting:
  image: python:3.11.4-slim
  stage: quality
  allow_failure: true
  before_script:
    - mkdir pylint
    - pip install --no-cache-dir -U flake8 pylint pylint-exit anybadge
  script:
    - flake8 --count --select=E9,F63,F7,F82 --show-source --statistics
    - flake8 --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics
    - pylint --output-format=text **/*.py | tee ./pylint/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint.log)
    - anybadge --label=Pylint --file=pylint/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 8=yellow 10=green
    - echo "Pylint score is $PYLINT_SCORE"
  when: manual
  artifacts:
    paths:
      - pylint/


analysis:
  image: python:3.11.4-slim
  stage: quality
  allow_failure: true
  before_script:
    - mkdir html
    - pip install --no-cache-dir bandit
  script:
    - bandit -c bandit.yaml -r **/*.py -f html -o html/bandit-report.html
  when: manual
  artifacts:
    when: always
    paths:
      - html/bandit-report.html

dependencycheck:
  stage: quality
  image:
    name: owasp/dependency-check
    entrypoint: [""]
  before_script:
    - mkdir html
  script:
    - /usr/share/dependency-check/bin/dependency-check.sh
      --scan . --format HTML --out html #  --enableExperimental --failOnCVSS 7
  when: manual
  artifacts:
    paths:
      - html


check:
  image: python:3.11.4-slim
  stage: quality
  before_script:
    - pip install --no-cache-dir -r requirements/dev.txt
    - apt-get update -qq && apt-get install -y sqlite3
  script:
    # Run checks to ensure the application is working fine
    - DJANGO_SETTINGS_MODULE=config.test_settings python manage.py check
  when: manual


prod:
  image: python:3.11.4-slim
  stage: prod
  when: manual # Continuous Delivery
  only:
    refs: [main]
  environment:
    name: production
    url: https://test.nskm.xyz
  script:
    - echo "This is a deploy step."
  when: manual
