from rest_framework import pagination


class PatientPagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'size'
    max_page_size = 50
    page_query_param = 'p'


class PlatePagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'size'
    max_page_size = 50
    page_query_param = 'p'
