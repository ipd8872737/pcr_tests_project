import secrets

from django.core.management.base import BaseCommand, CommandError
from faker import Faker

from apps.plates.models import TestType


class Command(BaseCommand):
    help = "Creates and inserts Types of PCR tests inside the database"

    def handle(self, *args, **options):
        objs = ({"name":"elisa idvet",
                 "number_of_whites": 1,
                 "number_of_negatives": 1,
                 "number_of_positives": 1},
                {"name":"elisa wantai",
                 "number_of_whites": 1,
                 "number_of_negatives": 3,
                 "number_of_positives": 2},
                {"name":"seroneutralization",
                 "number_of_whites": 1,
                 "number_of_negatives": 2,
                 "number_of_positives": 2},
        )
        for obj in objs:
            TestType.objects.create(**obj)
        self.stdout.write(
            self.style.SUCCESS('Successfully inserted types of PCR tests inside database')
        )
