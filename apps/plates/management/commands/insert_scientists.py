import secrets

from django.contrib.auth.models import Group, User
from django.core.management.base import BaseCommand, CommandError
from faker import Faker


class Command(BaseCommand):
    help = "Creates and inserts Scientists inside the database"

    def handle(self, *args, **options):
        groups = []
        for name in ("Immunology", "Epidemiology", "Virology", "Bacteriology"):
            group, _ = Group.objects.get_or_create(name=name)
            group.save()
            groups.append(group)

        fake = Faker()
        for i in range(3):
            usr = User.objects.create_user(
                username=fake.unique.user_name(),
                email=fake.unique.user_name(),
                password="pAssw0&d"
            )
            # add a user to a group
            usr.groups.add(groups[i])

        usr = User.objects.create_user(
            username="rbuckley",
            email=fake.unique.user_name(),
            password="pAssw0&d"
        )

        self.stdout.write(
            self.style.SUCCESS('Successfully inserted 7 scientists inside database')
        )
