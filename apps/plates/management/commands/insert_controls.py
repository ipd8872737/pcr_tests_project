import secrets

from django.core.management.base import BaseCommand, CommandError
from faker import Faker

from apps.plates.models import Control


class Command(BaseCommand):
    help = "Creates and inserts Controls inside the database"

    def handle(self, *args, **options):
        for x in ("BLANC", "POS", "NEG"):
            Control.objects.create(control_name=x)
        self.stdout.write(
            self.style.SUCCESS('Successfully inserted Controls inside database')
        )
