from django.apps import AppConfig


class PlatesAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.plates"
    label = "plates"
