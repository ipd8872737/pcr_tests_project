## Welcome

A powerful and flexible toolkit for playing with PCR plates.

## Overview

## Requirements

## Installation

## Project layout

    ├── apps
    │   ├── __init__.py
    │   └── plates
    │       ├── admin.py
    │       ├── apps.py
    │       ├── __init__.py
    │       ├── management
    │       │   └── commands
    │       │       ├── insert_controls.py
    │       │       ├── insert_locations.py
    │       │       ├── insert_patients.py
    │       │       ├── insert_scientists.py
    │       │       ├── insert_superuser.py
    │       │       └── insert_testtypes.py
    │       ├── migrations
    │       ├── models.py
    │       ├── pagination.py
    │       ├── serializers.py
    │       ├── tests
    │       │   ├── data
    │       │   ├── fixtures.py
    │       │   ├── __init__.py
    │       │   ├── tests_models.py
    │       │   └── tests_views.py
    │       ├── urls.py
    │       └── views.py
    ├── bandit.yaml
    ├── commons
    │   ├── __init__.py
    │   └── utils.py
    ├── compose.yaml
    ├── config
    │   ├── asgi.py
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── test_settings.py
    │   ├── test_wsgi.py
    │   ├── urls.py
    │   └── wsgi.py
    ├── Containerfile
    ├── env.sample
    ├── logs
    ├── manage.py
    ├── mkdocs.yml
    ├── pytest.ini
    ├── README.md
    ├── requirements
    
